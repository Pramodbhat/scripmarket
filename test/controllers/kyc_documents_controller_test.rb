require 'test_helper'

class KycDocumentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @kyc_document = kyc_documents(:one)
  end

  # test "should get index" do
  #   get kyc_documents_url
  #   assert_response :success
  # end

  # test "should get new" do
  #   get new_kyc_document_url
  #   assert_response :success
  # end

  # test "should create kyc_document" do
  #   assert_difference('KycDocument.count') do
  #     post kyc_documents_url, params: { kyc_document: { approved_by_id: @kyc_document.approved_by_id, approved_on: @kyc_document.approved_on, incorporation_cert: @kyc_document.incorporation_cert, null_cheque: @kyc_document.null_cheque, pan_card: @kyc_document.pan_card, phonenumber: @kyc_document.phonenumber, rejection_reason: @kyc_document.rejection_reason, status: @kyc_document.status, user_id: @kyc_document.user_id, valid_till: @kyc_document.valid_till } }
  #   end

  #   assert_redirected_to kyc_document_url(KycDocument.last)
  # end

  # test "should show kyc_document" do
  #   get kyc_document_url(@kyc_document)
  #   assert_response :success
  # end

  # test "should get edit" do
  #   get edit_kyc_document_url(@kyc_document)
  #   assert_response :success
  # end

  # test "should update kyc_document" do
  #   patch kyc_document_url(@kyc_document), params: { kyc_document: { approved_by_id: @kyc_document.approved_by_id, approved_on: @kyc_document.approved_on, incorporation_cert: @kyc_document.incorporation_cert, null_cheque: @kyc_document.null_cheque, pan_card: @kyc_document.pan_card, phonenumber: @kyc_document.phonenumber, rejection_reason: @kyc_document.rejection_reason, status: @kyc_document.status, user_id: @kyc_document.user_id, valid_till: @kyc_document.valid_till } }
  #   assert_redirected_to kyc_document_url(@kyc_document)
  # end

  # test "should destroy kyc_document" do
  #   assert_difference('KycDocument.count', -1) do
  #     delete kyc_document_url(@kyc_document)
  #   end

  #   assert_redirected_to kyc_documents_url
  # end
end
