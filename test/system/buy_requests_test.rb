require "application_system_test_case"

class BuyRequestsTest < ApplicationSystemTestCase
  setup do
    @buy_request = buy_requests(:one)
  end

  test "visiting the index" do
    visit buy_requests_url
    assert_selector "h1", text: "Buy Requests"
  end

  test "creating a Buy request" do
    visit buy_requests_url
    click_on "New Buy Request"

    check "Deleted" if @buy_request.deleted
    check "Sold" if @buy_request.sold
    fill_in "User", with: @buy_request.user_id
    click_on "Create Buy request"

    assert_text "Buy request was successfully created"
    click_on "Back"
  end

  test "updating a Buy request" do
    visit buy_requests_url
    click_on "Edit", match: :first

    check "Deleted" if @buy_request.deleted
    check "Sold" if @buy_request.sold
    fill_in "User", with: @buy_request.user_id
    click_on "Update Buy request"

    assert_text "Buy request was successfully updated"
    click_on "Back"
  end

  test "destroying a Buy request" do
    visit buy_requests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Buy request was successfully destroyed"
  end
end
