require "application_system_test_case"

class SellRequestsTest < ApplicationSystemTestCase
  setup do
    @sell_request = sell_requests(:one)
  end

  test "visiting the index" do
    visit sell_requests_url
    assert_selector "h1", text: "Sell Requests"
  end

  test "creating a Sell request" do
    visit sell_requests_url
    click_on "New Sell Request"

    fill_in "Created at", with: @sell_request.created_at
    fill_in "Rate", with: @sell_request.rate
    fill_in "Scrip date", with: @sell_request.scrip_date
    fill_in "Scrip number", with: @sell_request.scrip_number
    fill_in "Scrip value", with: @sell_request.scrip_value
    fill_in "User", with: @sell_request.user
    check "Verified" if @sell_request.verified
    click_on "Create Sell request"

    assert_text "Sell request was successfully created"
    click_on "Back"
  end

  test "updating a Sell request" do
    visit sell_requests_url
    click_on "Edit", match: :first

    fill_in "Created at", with: @sell_request.created_at
    fill_in "Rate", with: @sell_request.rate
    fill_in "Scrip date", with: @sell_request.scrip_date
    fill_in "Scrip number", with: @sell_request.scrip_number
    fill_in "Scrip value", with: @sell_request.scrip_value
    fill_in "User", with: @sell_request.user
    check "Verified" if @sell_request.verified
    click_on "Update Sell request"

    assert_text "Sell request was successfully updated"
    click_on "Back"
  end

  test "destroying a Sell request" do
    visit sell_requests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sell request was successfully destroyed"
  end
end
