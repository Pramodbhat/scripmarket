require "application_system_test_case"

class KycDocumentsTest < ApplicationSystemTestCase
  setup do
    @kyc_document = kyc_documents(:one)
  end

  test "visiting the index" do
    visit kyc_documents_url
    assert_selector "h1", text: "Kyc Documents"
  end

  test "creating a Kyc document" do
    visit kyc_documents_url
    click_on "New Kyc Document"

    fill_in "Approved by", with: @kyc_document.approved_by_id
    fill_in "Approved on", with: @kyc_document.approved_on
    fill_in "Incorporation cert", with: @kyc_document.incorporation_cert
    fill_in "Null cheque", with: @kyc_document.null_cheque
    fill_in "Pan card", with: @kyc_document.pan_card
    fill_in "Phonenumber", with: @kyc_document.phonenumber
    fill_in "Rejection reason", with: @kyc_document.rejection_reason
    fill_in "Status", with: @kyc_document.status
    fill_in "User", with: @kyc_document.user_id
    fill_in "Valid till", with: @kyc_document.valid_till
    click_on "Create Kyc document"

    assert_text "Kyc document was successfully created"
    click_on "Back"
  end

  test "updating a Kyc document" do
    visit kyc_documents_url
    click_on "Edit", match: :first

    fill_in "Approved by", with: @kyc_document.approved_by_id
    fill_in "Approved on", with: @kyc_document.approved_on
    fill_in "Incorporation cert", with: @kyc_document.incorporation_cert
    fill_in "Null cheque", with: @kyc_document.null_cheque
    fill_in "Pan card", with: @kyc_document.pan_card
    fill_in "Phonenumber", with: @kyc_document.phonenumber
    fill_in "Rejection reason", with: @kyc_document.rejection_reason
    fill_in "Status", with: @kyc_document.status
    fill_in "User", with: @kyc_document.user_id
    fill_in "Valid till", with: @kyc_document.valid_till
    click_on "Update Kyc document"

    assert_text "Kyc document was successfully updated"
    click_on "Back"
  end

  test "destroying a Kyc document" do
    visit kyc_documents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Kyc document was successfully destroyed"
  end
end
