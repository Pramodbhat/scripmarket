require 'test_helper'

class UserAttributeTest < ActiveSupport::TestCase

  test "Test can? for default" do
    user = User.new
    user.email = "p@123.com"
    user.password = "abcd1234"
    if !user.save 
      puts user.errors.full_messages
      assert false, "Failed to save user"
    end
    
    attributes = UserAttribute.new
    attributes.user = user
    if !attributes.save
      puts attributes.errors.full_messages
      assert false, "Failed to save attributes"
    end
    assert UserAttribute.can?(user.id, 'sell'), "user should be able sell by default"
    assert_not UserAttribute.can?(user.id, 'buy'), "user should not be able to buy by default"
    assert_not UserAttribute.can?(user.id, 'user_admin'), "user should not be user_admin by default"
    assert_not UserAttribute.can?(user.id, 'verify_kyc'), "user should not verify_kyc by default"
    assert_not UserAttribute.can?(user.id, 'verify_scrip'), "user should not verify_scrip by default"
    assert_not UserAttribute.can?(user.id, 'sell_scrip'), "user should not sell_scrip by default"

    assert attributes.can?('sell'), "user should be able sell by default"
    assert_not attributes.can?('buy'), "user should not be able to buy by default"
    assert_not attributes.can?('user_admin'), "user should not be user_admin by default"
    assert_not attributes.can?('verify_kyc'), "user should not verify_kyc by default"
    assert_not attributes.can?('verify_scrip'), "user should not verify_scrip by default"
    assert_not attributes.can?('sell_scrip'), "user should not sell_scrip by default"

  end

  test "find_or_create associates user to attributes" do
    user = User.new
    user.email = "p@123.com"
    user.password = "abcd1234"
    user.save

    attributes = UserAttribute.find_or_create(user.id)
    assert attributes, "attributes is nil"

    previous_attributes = attributes
    attributes = UserAttribute.find_or_create(user.id)
    assert_equal previous_attributes.id, attributes.id, "find_or_create should only create new attributes once"
  end
  # test "the truth" do
  #   assert true
  # end
end
