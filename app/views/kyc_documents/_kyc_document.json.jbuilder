json.extract! kyc_document, :id, :pan_card, :null_cheque, :incorporation_cert, :user_id, :approved_by_id, :status, :approved_on, :rejection_reason, :valid_till, :phonenumber, :created_at, :updated_at
json.url kyc_document_url(kyc_document, format: :json)
