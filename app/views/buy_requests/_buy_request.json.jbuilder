json.extract! buy_request, :id, :user_id, :deleted, :sold, :created_at, :updated_at
json.url buy_request_url(buy_request, format: :json)
