json.extract! sell_request, :id, :user, :created_at, :scrip_number, :scrip_date, :scrip_value, :rate, :verified, :created_at, :updated_at
json.url sell_request_url(sell_request, format: :json)
