class BuySellBindingController < ApplicationController
    before_action :authenticate_user!
    before_action :check_for_permissions
    before_action :setup
    before_action :set_buy_sell_binding_request, only: [:destroy]
    rescue_from ActionController::RoutingError, with: :user_not_authorized

    def setup
        @user = current_user
    end
    
    def user_not_authorized
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end  
  
    def check_for_permissions
        if !current_user.user_attributes.can?("buy")
            if ENV["ADMIN_EMAILS"] and !ENV["ADMIN_EMAILS"].split(',').include?(current_user.email)
                raise ActionController::RoutingError.new("Forbidden")
            end
        end
    end
    
    def create
        @buy_sell_binding = BuySellBinding.new()
        @buy_sell_binding.sell_request_id = buy_sell_binding_params["sell_request_id"]
        @buy_sell_binding.buy_request_id = buy_sell_binding_params["buy_request_id"]
        @buy_sell_binding.buy_commission =  BusinessConfig.get_latest(current_user.id).config["buy_commission"].to_i
        @selected_sale_price = 0

        if BuySellBinding.where(sell_request_id: buy_sell_binding_params["sell_request_id"].to_i,buy_request_id: buy_sell_binding_params["buy_request_id"].to_i).present?
            flash[:alert] = "Can't select duplicate Scrip.Please reselect scrip"
            redirect_to :dashboard_verify_buy
            return false
        end
        if @buy_sell_binding.save
         sell_request = SellRequest.find buy_sell_binding_params["sell_request_id"] 
         buy_request = BuyRequest.find buy_sell_binding_params["buy_request_id"]
         buy_request.total_selected_scrip_value +=  (sell_request.scrip_value.to_d)
         total_sale_rate = sell_request.rate.to_d + @buy_sell_binding.buy_commission.to_d
         buy_request.total_selected_scrip_sale_price +=  (sell_request.scrip_value.to_d*total_sale_rate/100)
         buy_request.save
            flash[:notice] = "Sell request successfully selected"
            redirect_to buy_sell_binding_params["referrer"]
        else
            flash[:alert] = "Sell request selection failed"
            redirect_to buy_sell_binding_params["referrer"]
        end
    
    end

    def destroy
        if !current_user.is_super_admin? && @buy_sell_binding.buy_request.user_id != current_user.id
            if  @user.role != "ops"
                raise ActionController::RoutingError.new("Forbidden")
                return
            end
        end
         buy_sell_binding =     BuySellBinding.find params[:id]
         sell_request_id = buy_sell_binding.sell_request_id
         buy_request_id = buy_sell_binding.buy_request_id

        if @buy_sell_binding.destroy
          sell_request = SellRequest.find sell_request_id 
         buy_request = BuyRequest.find buy_request_id
         buy_request.total_selected_scrip_value -=  (sell_request.scrip_value.to_d)
         total_sale_rate = sell_request.rate.to_d + @buy_sell_binding.buy_commission.to_d
         buy_request.total_selected_scrip_sale_price -=  (sell_request.scrip_value.to_d*total_sale_rate/100)
         buy_request.save
            flash[:notice] = "Sell request successfully deselected"
            redirect_to buy_sell_binding_params["referrer"]
        else
            flash[:alert] = "Sell request deselection failed"
            redirect_to buy_sell_binding_params["referrer"]
        end
    end

    def buy_sell_binding_params
        params.permit(:sell_request_id, :buy_request_id, :referrer, :id)
    end

    private
    def set_buy_sell_binding_request
        @buy_sell_binding = BuySellBinding.find(buy_sell_binding_params["id"])
    end
end
