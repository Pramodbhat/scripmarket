class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  def logout
    sign_out_and_redirect(current_user)
  end

  protected

  def after_sign_in_path_for(resource)
    # return the path based on resource
    # UserAttribute.find_or_create(current_user.id)
      dashboard_home_path
    # if current_user.role == "user"
    #   if current_user.kyc_approved
    #   else
    #     new_kyc_document_path
    #   end
    # elsif current_user.role == "ops"
    #   dashboard_home_path
    # end
  end

end
