class VerifySellController < ApplicationController
    before_action :authenticate_user!
    before_action :check_for_permissions
    before_action :setup
    layout "application"
    rescue_from ActionController::RoutingError, with: :user_not_authorized

    def setup
        @user = current_user
    end

    def user_not_authorized
        render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end

    def check_for_permissions
        if !current_user.user_attributes.can?("verify_scrip")
            if ENV["ADMIN_EMAILS"] and !ENV["ADMIN_EMAILS"].split(',').include?(current_user.email)
                raise ActionController::RoutingError.new("Forbidden")
            end
        end
    end

    def show
        @sell_request = SellRequest.find(params['id'])
        @kyc_document = @sell_request.user.kyc_documents.where("status = ?","APPROVED").first
    end

    def kyc_document_map
        map_data = {}
        @verifications.each do |verification|
            verification.user.kyc_documents.select do |kyc_document|
                if kyc_document.status.eql?("APPROVED")
                    map_data[verification.user.id] = kyc_document
                else
                    map_data[verification.user.id] = KycDocument.new 
                end
            end
        end
        map_data
    end

    def index
        @kyc_document_map = {}
        @verifications = SellRequest.for_verification
        @kyc_document_map = kyc_document_map
    end

    def rejections
        @kyc_document_map = {}
        @verifications = SellRequest.rejected_sales
        @kyc_document_map = kyc_document_map
    end

    def reject
        @sell_request = SellRequest.find(params['verify_sell_id'])
        reason = params['rejection_reason']
        if reason.nil? or reason.empty?
            flash[:alert] = "Reason cannot be empty"
            redirect_to action: :show, id:  @sell_request.id
            return
        end

        if @sell_request.sold or @sell_request.deleted or @sell_request.verified
            flash[:alert] = "Sell request is already sold, deleted or verified"
            redirect_to action: :show, id:  @sell_request.id
            return
        end
        @sell_request.rejected = true
        @sell_request.rejection_reason = reason
        @sell_request.save!
        flash[:notice] = "Success"
        redirect_to action: :show, id:  @sell_request.id
    end

    def verify
        @sell_request = SellRequest.find(params['verify_sell_id'])
        if @sell_request.sold or @sell_request.deleted or @sell_request.rejected
            flash[:alert] = "Sell request is already sold, deleted or rejected"
            redirect_to action: :show, id:  @sell_request.id
            return
        end
        @sell_request.verified_by = current_user.id
        @sell_request.verified = true
        @sell_request.save!
        flash[:notice] = "Success"
        redirect_to action: :show, id:  @sell_request.id
    end

    def request_lock
      @sell_request = SellRequest.find(params['verify_sell_id'])
      @sell_request.locked_by_user_id = current_user[:id]
      @sell_request.is_locked = true
      if @sell_request.save
        flash[:alert] = "Sell request is Locked"
        redirect_to action: :show, id:  @sell_request.id
        return
      end
    end
end
