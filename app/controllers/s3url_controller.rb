class S3urlController < ApplicationController
  before_action :authenticate_user!

  def extension (fname)
    fname.split(".").last
  end

  def index
    respond_to do |format|
      format.json {
        key = params["type"]+"_"+SecureRandom.hex(10) + '.'+ extension(params["filename"])
        post = Aws::S3::PresignedPost.new(Aws.config[:credentials],Aws.config[:region],
                                          "scripmarket-kyc").key(key)
        render json: {"url" => post.url, "data" => post.fields, "key" => key}
      }
    end
  end
end
