class BuyRequestsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_permissions
  before_action :setup
  before_action :set_buy_request, only: [:show, :edit, :update, :destroy]
  rescue_from ActionController::RoutingError, with: :user_not_authorized

  def setup
    @user = current_user
  end

  def user_not_authorized
    render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
  end

  def check_for_permissions
    if !current_user.user_attributes.can?("buy")
        if ENV["ADMIN_EMAILS"] and !ENV["ADMIN_EMAILS"].split(',').include?(current_user.email)
            raise ActionController::RoutingError.new("Forbidden")
        end
    end
  end

  # GET /buy_requests
  # GET /buy_requests.json
  def index
    @buy_requests = BuyRequest.all
  end

  # GET /buy_requests/1
  # GET /buy_requests/1.json
  def show
  end

  # GET /buy_requests/new
  def new
    @buy_request = BuyRequest.new
  end

  # GET /buy_requests/1/edit
  def edit
  end

  # POST /buy_requests
  # POST /buy_requests.json
  def create
    @buy_request = BuyRequest.new(buy_request_params)
    # convert amount to cents
    @buy_request.scrip_value = @buy_request.scrip_value * 100
    @buy_request.user = current_user
    respond_to do |format|
      if @buy_request.save
        format.html { redirect_to dashboard_select_scrips_user_path(buy_id: @buy_request.id), notice: 'Buy request was successfully created.' }
        format.json { render :show, status: :created, location: @buy_request }
      else
        format.html { render :new }
        format.json { render json: @buy_request.errors, status: :unprocessable_entity }
      end
    end
  end

  def submit
    @buy_request = BuyRequest.find(params[:buy_request_id])
    puts @buy_request.submitted
    @buy_request.submitted = true
    if @buy_request.save
      flash[:notice] = "Buy Request was successfully submitted"
      redirect_to dashboard_buy_path
    else
      errors = @buy_request.errors.full_messages
      flash[:alert] = "Buy Request submission failed:" + errors.join(",")
      redirect_to dashboard_select_scrips_user_path(buy_id: @buy_request.id)
    end
  end

  # PATCH/PUT /buy_requests/1
  # PATCH/PUT /buy_requests/1.json
  def update
    if @buy_request.sold
      redirect_to dashboard_buy_path, notice: 'Cant update sold buy request'
      return
    end
    buy_update = buy_request_params
    buy_update["scrip_value"] = (buy_update["scrip_value"].to_d*100).to_i.to_s
    respond_to do |format|

      if @buy_request.update(buy_update)
        format.html { redirect_to dashboard_buy_path, notice: 'Buy request was successfully updated.' }
        format.json { render :show, status: :ok, location: @buy_request }
      else
        format.html { render :edit }
        format.json { render json: @buy_request.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def audit
    @buy_request = BuyRequest.where(id:params[:buy_request_id].to_i).last
    @buy_request.audited_by = current_user[:id]
    @buy_request.is_auidted = true
    if @buy_request.save(validate:false)
      flash[:alert] = "Buy Request Successfully Audited"
    end

          return redirect_to dashboard_buy_path

  end 

  def audit_reject
    @buy_request = BuyRequest.where(id:params[:buy_request_id].to_i).last
    @buy_request.audited_by = current_user[:id]
    @buy_request.is_auidted = true
    @buy_request.audit_rejection_message = params[:message] || "Admin Rejected the Audit"
    @buy_request.is_audit_rejected = true
    @buy_request.save(validate:false)
    if @buy_request.save 
      respond_to do |format|
        format.html { redirect_to dashboard_buy_path, notice: 'Buy request was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to dashboard_buy_path, notice: 'Failed to destroy Buy Request.' }
        format.json { head :no_content }
      end  
    end


  end

  # DELETE /buy_requests/1
  # DELETE /buy_requests/1.json
  def destroy
    if @buy_request.sold
      redirect_to dashboard_buy_path, notice: 'Cant delete sold buy request'
      return
    end

    @buy_request.deleted = true
    if @buy_request.save 
      respond_to do |format|
        format.html { redirect_to dashboard_buy_path, notice: 'Buy request was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to dashboard_buy_path, notice: 'Failed to destroy Buy Request.' }
        format.json { head :no_content }
      end  
    end
  end

  def is_sold
    buy_requests = BuyRequest.where(id:params[:buy_request_id].to_i).last
    if buy_requests.present?
      selected_sold_sell_request_id = buy_requests.sell_requests.map(&:id)
      buy_requests.sold = true
      buy_requests.sold_user_id = current_user[:id]
      buy_requests.sold_user_email = current_user[:email]
      buy_requests.sold_date = Time.now.strftime("%d/%m/%Y %H:%M")
      buy_requests.save!  
      SellRequest.where(id:selected_sold_sell_request_id).update_all(sold: true)
      buy_sell_buynding = BuySellBinding.where("sell_request_id in (?) and  buy_request_id != ?",selected_sold_sell_request_id,params[:buy_request_id].to_i)
      buy_sell_buynding.each do |bs|
        bs.delete
      end
    end  
    redirect_to dashboard_buy_path
  end

  def lock_buy
    buy_request = BuyRequest.find params[:buy_request_id]
    buy_request.locked_by_user_id = current_user[:id]
    buy_request.is_locked = true
    if buy_request.save(validate:false)
      flash[:alert] = "Buy Request Successfully Locked"
      redirect_to :dashboard_verify_buy
    end  
  end 

  def reject_buy
    buy_request = BuyRequest.find params[:buy_request_id]
    buy_request.submitted = false;
    if buy_request.save(validate:false)
      flash[:alert] = "Buy Request Successfully Un Sumbitted Stage"
      redirect_to :dashboard_verify_buy
    end  
  end
 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_buy_request
      @buy_request = BuyRequest.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def buy_request_params
      params.require(:buy_request).permit(:user_id, :deleted, :sold, :buy_date, :scrip_value, :referrer,:descrption)
    end
end
