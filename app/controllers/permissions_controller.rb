class PermissionsController < ApplicationController
    before_action :authenticate_user!
    before_action :setup
    before_action :check_for_permissions
    layout "application"
    rescue_from ActionController::RoutingError, with: :user_not_authorized

    def setup
        @user = current_user
        @permission_list=[:buy, :sell, :verify_scrip, :verify_kyc, :verify_buy, :audit]
        @role_list=[['User', 'user'],['Ops', 'ops']]
    end

    def user_not_authorized
        render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end

    def check_for_permissions
        if !current_user.user_attributes.can?("user_admin")
            if ENV["ADMIN_EMAILS"] and !ENV["ADMIN_EMAILS"].split(',').include?(current_user.email)
                raise ActionController::RoutingError.new("Forbidden")
            end
        end
    end

    def index
        @users = User.order(email: :asc)
    end

    def edit
        @user = User.find_by(id: params["id"])
    end

    def update
        @user = User.find_by(id: params["id"])
        attributes = @user.user_attributes
        @permission_list.each do |permission|
            if params.key?(permission)
                attributes.preferences[permission.to_s] = true
            else
                attributes.preferences[permission.to_s] = false
            end
        end
        @user.role = params["role"]
        @user.save!
        if attributes.save!
          flash[:alert] = params["role"] + "  Permissions " + +"Successfully Updated"
        end
        redirect_to :action => "edit", :id => @user.id
    end
end
