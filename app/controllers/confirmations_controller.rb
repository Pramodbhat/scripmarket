class ConfirmationsController < Devise::ConfirmationsController

  private
  def after_confirmation_path_for(resource_name, resource)
    sign_in(resource) # In case you want to sign in the user
    UserAttribute.find_or_create(current_user.id)
    if current_user.role == "user"
      if current_user.kyc_approved
        dashboard_home_path
      else
        new_kyc_document_path
      end
    elsif current_user.role == "ops"
      dashboard_home_path
    end  
  end
end