require 'bigdecimal'
require 'bigdecimal/util'

class SellRequestsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_permissions
  before_action :setup
  before_action :set_sell_request, only: [:show, :edit, :update, :destroy, :upload_invoice]
  rescue_from ActionController::RoutingError, with: :user_not_authorized

  def user_not_authorized
    render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
  end

  def check_for_permissions
    # if !current_user.user_attributes.can?("sell")
    #     if ENV["ADMIN_EMAILS"] and !ENV["ADMIN_EMAILS"].split(',').include?(current_user.email)
    #         raise ActionController::RoutingError.new("Forbidden")
    #     end
    # end
  end

  def setup
    @user = current_user
  end

  # GET /sell_requests
  # GET /sell_requests.json
  def index
    @sell_requests = SellRequest.all
  end

  # GET /sell_requests/1
  # GET /sell_requests/1.json
  def show
  end

  # GET /sell_requests/new
  def new
    @business_config = BusinessConfig.get_latest(current_user.id)
    @sell_request = SellRequest.new
  end

  # GET /sell_requests/1/edit
  def edit
    @business_config = BusinessConfig.get_latest(current_user.id)
    @sell_request = SellRequest.find(params[:id])
  end

  def calculate_commission(scrip_value, rate)
    config = BusinessConfig.get_latest(current_user.id).config
    commission = 20
    config["commission"].each do |slab|
      if scrip_value >= slab["min"] and ((scrip_value < slab["max"]) or (slab["max"] == -1))
        commission = slab["value"]
      end
    end
    commission
  end

  # POST /sell_requests
  # POST /sell_requests.json
  def create
        @business_config = BusinessConfig.get_latest(current_user.id)

    @sell_request = SellRequest.new(sell_request_params)
    @sell_request.user_id = current_user.id
    @sell_request.verified = false
    @sell_request.rate = (sell_request_params["rate"].to_d*100).to_i
    @sell_request.scrip_value = @sell_request.scrip_value*100
    @sell_request.commission = calculate_commission(@sell_request.scrip_value, @sell_request.rate)

    respond_to do |format|
      if @sell_request.save
        format.html { redirect_to dashboard_sell_path, notice: 'Sell request was successfully created.' }
        format.json { render :show, status: :created, location: @sell_request }
      else
        format.html { render :new }
        format.json { render json: @sell_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sell_requests/1
  # PATCH/PUT /sell_requests/1.json
  def update
    if @sell_request.user_id != current_user.id and @sell_request.role == "user"
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return
    end
    sell_update = sell_request_params
    sell_update["rate"] = (sell_update["rate"].to_d*100).to_i
    sell_update["scrip_value"] = (sell_update["scrip_value"].to_i*100).to_s
    sell_update["commission"] = calculate_commission(sell_update["scrip_value"].to_i, sell_update["rate"].to_i)

    respond_to do |format|
      if @sell_request.update(sell_update)
        format.html { redirect_to dashboard_sell_path, notice: 'Sell request was successfully updated.' }
        format.json { render :edit, status: :ok, location: @sell_request }
      else
        format.html { render :edit }
        format.json { render json: @sell_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /sell_requests/1/resubmit
  def resubmit
    @sell_request = SellRequest.find(params[:sell_request_id])
    if @sell_request.user_id != current_user.id and @sell_request.role == "user"
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return
    end

    if @sell_request.sold or @sell_request.verified or !@sell_request.rejected
      flash[:alert] = "Failed to resubmit issue"
      redirect_to params["referrer"]
    end

    @sell_request.rejected = false
    if @sell_request.save
      redirect_to params["referrer"]
      return
    else
      flash[:alert] = 'Failed to resubmit sell request'
      redirect_to params["referrer"]
    end
  end

  # DELETE /sell_requests/1
  # DELETE /sell_requests/1.json
  def destroy
    if @sell_request.sold
      flash[:alert] = 'Sold sell_requests cannot be deleted.'
      redirect_to params["referrer"]
      return  
    end

    if current_user.role == "user" and @sell_request.verified
      flash[:alert] = 'Verified sell_requests cannot be deleted.'
      redirect_to params["referrer"]
      return  
    end

    @sell_request.deleted = true
    if @sell_request.save
      flash[:notice] = 'Sell request was successfully deleted.'
      redirect_to params["referrer"]
      return
    end
    flash[:alert]
    redirect_to params["referrer"]
  end

   def upload_invoice
        @business_config = BusinessConfig.get_latest(current_user.id)
    @sell_request = SellRequest.find(params[:sell_request_id])

   end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sell_request
      @sell_request = params[:id].present? ? SellRequest.find(params[:id]) : SellRequest.find(params[:sell_request_id])
    end

    # Only allow a list of trusted parameters through.
    def sell_request_params
      params.require(:sell_request).permit(:scrip_number, :scrip_date, :scrip_value, :rate, :commission)
    end
end
