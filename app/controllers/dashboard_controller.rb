class DashboardController < ApplicationController
  before_action :authenticate_user!
  before_action :setup
  layout "application"

  def setup
    @hide_side_bar = false
    @user = current_user 
    @side_menu = []
    {
      "sell" => [{name: "Place Sell Request", target: new_sell_request_path}],
      "buy" => [{name: "Place Buy Request", target: new_buy_request_path}]
    }.each do |k,v|
      if current_user.can?(k)
        v.each { |button| @side_menu.push(button) }
      end
    end
    @side_menu.push({name: "Invoices", target: dashboard_home_path})
    @side_menu.push({name: "Logout", target: logout_path})
  end

  def home
    @dashboard_home_buttons = []
    if current_user.can?("sell")
      @dashboard_home_buttons.push({src: "https://scripmarket-public.s3.ap-south-1.amazonaws.com/sell_button.png",
                                url: dashboard_sell_path})
    end
    if current_user.can?("buy")
      @dashboard_home_buttons.push({src: "https://scripmarket-public.s3.ap-south-1.amazonaws.com/buy_button.png",
                                url: dashboard_buy_path})
    end
    if current_user.can?("user_admin") or current_user.is_super_admin?
      @dashboard_home_buttons.push({src: "https://scripmarket-public.s3.ap-south-1.amazonaws.com/permissions_button.png",
                                url: permissions_path})
    end
    if current_user.can?("verify_scrip")
      @dashboard_home_buttons.push({src: "https://scripmarket-public.s3.ap-south-1.amazonaws.com/verify_sell.png",
                                url: verify_sell_index_path})
    end
    if current_user.can?("verify_kyc") and current_user.role == "ops"
      @dashboard_home_buttons.push({src: "https://scripmarket-public.s3.ap-south-1.amazonaws.com/verify_kyc.png",
      url: kyc_documents_path})
    end
    if current_user.is_super_admin?
      @dashboard_home_buttons.push({src: "https://scripmarket-public.s3.ap-south-1.amazonaws.com/business_config.png",
      url: dashboard_business_config_path})
    end

    if current_user.can?("verify_buy") and current_user.role == "ops"
      @dashboard_home_buttons.push({src: "https://scripmarket-public.s3.ap-south-1.amazonaws.com/verify_buy_button.png",
      url: dashboard_verify_buy_path})
    end
    @current_user_id = current_user.id
    @current_user_email = current_user.email
    @current_user_name =  current_user.kyc_documents.last[:name]
    @current_user_role = current_user.role
    @total_sell_request_count =   current_user.is_super_admin? ? SellRequest.all.count  : SellRequest.where("user_id = ?",@current_user_id).count
    @total_sell_verified_request_count = current_user.is_super_admin? ? SellRequest.where("verified = true").count : SellRequest.where("user_id = ? AND verified = true",@current_user_id).count
   

    @total_sell_un_verified_request_count = current_user.is_super_admin? ? SellRequest.where("verified = false").count : SellRequest.where("user_id = ? AND verified = false",@current_user_id).count
   
    @total_buy_request_count = current_user.is_super_admin? ? BuyRequest.all.count : BuyRequest.where("user_id = ?",@current_user_id).count
   
    if current_user.is_super_admin? 
      @total_buy_request_verified_sold_count = BuyRequest.where("sold = true").count
     
      sell_request = SellRequest.where("verified = true AND deleted = false")
     
      @sell_request_sold = SellRequest.where(" verified = true AND sold = true AND deleted = false",).count


    else
      @total_buy_request_verified_sold_count = BuyRequest.where("user_id = ? AND sold = true",@current_user_id).count
     
      sell_request = SellRequest.where("user_id = :user_id AND verified = true AND deleted = false", {user_id: @current_user_id})
     
      @sell_request_sold = SellRequest.where("user_id = :user_id AND verified = true AND sold = true AND deleted = false", {user_id: @current_user_id}).count
    end
    @total_savings = 0
    sell_request.each do |sale|
    @total_savings +=  ((sale.rate.to_d/10000) - (sale.commission.to_d/10000))*(sale.scrip_value.to_d/100)
    end
    @buy_request = current_user.is_super_admin? ? BuyRequest.all : BuyRequest.where("user_id = ?",@current_user_id)
    @selected_sell_details = []
    @available_sell_details = []
    @selected_total_value = 0
    @selected_total_sale_price = 0
    @total_earnings = 0
    @selected_sale_price = 0
    @buy_request.each do |br|
      br.sell_requests.each do |sell_request|
        binding = BuySellBinding.find_by(sell_request_id: sell_request.id, buy_request_id: br.id)
        sale = { :sale => sell_request,
                  :binding => binding }
        @selected_sell_details.append(sale)
        @selected_total_value += sell_request.scrip_value.to_d
        total_sale_rate = sell_request.rate.to_d + binding.buy_commission.to_d
        @selected_sale_price += (sell_request.scrip_value.to_d*total_sale_rate/100)
        @selected_total_sale_price += (sell_request.scrip_value.to_d - (@selected_sale_price))
      end
    end  
    @total_earnings = @selected_total_value/100 - @selected_sale_price/10000
  end

  def buy
    if !current_user.can?("buy")
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return 
    end
    @active_buys = BuyRequest.active_buys(current_user)
    @completed_buys = BuyRequest.completed_buys(current_user)
  end

  def sell
    if !current_user.can?("sell")
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return 
    end
    @active_sales = SellRequest.active_sales(current_user.id)
    @unverified_sales = SellRequest.unverified_sales(current_user.id)
    @completed_sales = SellRequest.completed_sales(current_user.id)
    @current_user = current_user
  end

  def business_config

    if !["pramodbha8@gmail.com"].split(',').flatten.include?(current_user.email)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return
    end 
    @business_config = BusinessConfig.get_latest(current_user.id)
  end

  def select_scrips_user
    if !current_user.can?("buy")
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return 
    end

    buy_id = params["buy_id"]
    @buy_request = BuyRequest.find(buy_id)
    if @buy_request == nil
      flash[:alert] = "Could not find Buy Request"
      redirect_to :dashboard_path
      return
    end

    # if @buy_request.user.id != current_user.id 
    #   render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    #   return 
    # end

    @selected_sell_details = []
    @available_sell_details = []
    @selected_total_value = 0
    @selected_total_sale_price = 0
    sell_ids = {}
    @selected_sale_price = 0
    @buy_request.sell_requests.each do |sell_request|
      binding = BuySellBinding.find_by(sell_request_id: sell_request.id, buy_request_id: @buy_request.id)
      sale = { :sale => sell_request,
                :binding => binding }
      @selected_sell_details.append(sale)
      @selected_total_value += sell_request.scrip_value.to_d
      total_sale_rate = sell_request.rate.to_d + binding.buy_commission.to_d
      @selected_sale_price += (sell_request.scrip_value.to_d*total_sale_rate/100)
      @selected_total_sale_price += (sell_request.scrip_value.to_d - (@selected_sale_price))
      sell_ids[sell_request.id] = true
    end

    SellRequest.active_sales_all.each do |sell_request|
      if !sell_ids.key?(sell_request.id)
        sale = { :sale => sell_request,
        :binding => nil }
        @available_sell_details.append(sale)
      end
    end

    @business_config = BusinessConfig.get_latest(current_user.id)
  end

  def verify_buy
    allowed = current_user.can?("verify_buy")
    allowed = allowed and current_user.role == "ops"

    if !allowed 
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return 
    end

    @active_buys = BuyRequest.active_buys_submitted
  end

  def audit_dashboard
    @open_audit = BuyRequest.open_audit(current_user)
    @accepted_audit = BuyRequest.accepted_audit(current_user)
    @rejected_audit = BuyRequest.rejected_audit(current_user)
    
  end

  def procees_to_sell
  end

  def select_scrips_ops
    allowed = current_user.can?("verify_buy")
    allowed = allowed and current_user.role == "ops"

    if !allowed 
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return 
    end

    buy_id = params["buy_id"]
    @buy_request = BuyRequest.find(buy_id)
    if @buy_request == nil
      flash[:alert] = "Could not find Buy Request"
      redirect_to :dashboard_path
      return
    end

    @buy_phonenumber = ""
    @buy_name = ""
    @buy_request.user.kyc_documents.select do |kyc_document|
      @buy_name = kyc_document.name
      @buy_phonenumber = kyc_document.phonenumber
    end
 
    @selected_sell_details = []
    @available_sell_details = []
    @selected_total_value = 0
    @selected_total_sale_price = 0
    @selected_sale_price = 0
    sell_ids = {}
    @buy_request.sell_requests.each do |sell_request|
      binding = BuySellBinding.find_by(sell_request_id: sell_request.id, buy_request_id: @buy_request.id)
      phonenumber = ""
      name = ""
      sell_request.user.kyc_documents.select do |kyc_document|
        if kyc_document.status.eql?("APPROVED") || kyc_document.status.eql?("SUBMITTED")
          name = kyc_document.name
          phonenumber = kyc_document.phonenumber
        end
      end

      sale = { :sale => sell_request,
                :binding => binding,
                :name => name,
                :phonenumber => phonenumber }

      @selected_sell_details.append(sale)
      @selected_total_value += sell_request.scrip_value.to_d
      total_sale_rate = sell_request.rate.to_d + binding.buy_commission.to_d
      @selected_sale_price  += (sell_request.scrip_value.to_d*total_sale_rate/10000)
      @selected_total_sale_price += sell_request.scrip_value.to_d - (@selected_sale_price)
      sell_ids[sell_request.id] = true
    end

    SellRequest.active_sales_all.each do |sell_request|
      if !sell_ids.key?(sell_request.id)
        phonenumber = ""
        name = ""
        sell_request.user.kyc_documents.select do |kyc_document|
          if kyc_document.status.eql?("APPROVED")
            name = kyc_document.name
            phonenumber = kyc_document.phonenumber
          end
        end
  
        sale = { :sale => sell_request,
        :binding => nil,
        :name => name,
        :phonenumber => phonenumber}
        @available_sell_details.append(sale)
      end
    end

    @business_config = BusinessConfig.get_latest(current_user.id)

  end
  def invoices
    @buys = {}
    @completed_buy = BuyRequest.completed_buys(current_user)
    @completed_buy.each do |cb|
       key = cb.id
       @buys[key] = {}  if @buys[key].blank?
       @buys[key][:user_id] = cb.user_id
       @buys[key][:deleted] = cb.deleted
       @buys[key][:sold] = cb.sold
       @buys[key][:created_at] = cb.created_at
       @buys[key][:buy_date] = cb.buy_date
       @buys[key][:sold_date] = cb.sold_date
       @buys[key][:scrip_value] = cb.scrip_value
       @buys[key][:descrption] = cb.descrption
       @buys[key][:selected_total_value] = 0
       @buys[key][:selected_total_sale_price] = 0
       cb.sell_requests.each do |sell_request|
         binding = BuySellBinding.find_by(sell_request_id: sell_request.id, buy_request_id: cb.id)
         @buys[key][:selected_total_value] += sell_request.scrip_value.to_d
         total_sale_rate = sell_request.rate.to_d + binding.buy_commission.to_d
         @buys[key][:selected_total_sale_price] += sell_request.scrip_value.to_d - (sell_request.scrip_value.to_d*total_sale_rate/100)
        end
    end
  end

   def download_pdf
    @user = User.order("id DESC").all
    buy_id = params["buy_id"][0].to_i

    @buy_request = BuyRequest.find(buy_id)
    if @buy_request == nil
      flash[:alert] = "Could not find Buy Request"
      redirect_to :dashboard_path
      return
    end
    if @buy_request.user.id != current_user.id 
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return 
    end
    @selected_sell_details = []
    @available_sell_details = []
    @selected_total_value = 0
    @selected_total_sale_price = 0
    @selected_total_sale_value = 0
    sell_ids = {}
    @selected_sale_price = 0
    @buy_request.sell_requests.each do |sell_request|
      binding = BuySellBinding.find_by(sell_request_id: sell_request.id, buy_request_id: @buy_request.id)
      sale = { :sale => sell_request,
                :binding => binding }
      @selected_sell_details.append(sale)
      @selected_total_value += sell_request.scrip_value.to_d
      total_sale_rate = sell_request.rate.to_d + binding.buy_commission.to_d
      @selected_total_sale_price += sell_request.scrip_value.to_d - (sell_request.scrip_value.to_d*total_sale_rate/10000)
      # @selected_total_sale_value += sell_request.scrip_value.to_d - @selected_sale_price
      # sell_ids[sell_request.id] = true
      @selected_sale_price  += (sell_request.scrip_value.to_d*total_sale_rate/10000)
      # @selected_total_sale_price += sell_request.scrip_value.to_d - (@selected_sale_price)
  
    end
    @business_config = BusinessConfig.get_latest(current_user.id)
    br = BuyRequest.find buy_id
    br.tcs_commision = 0.075
    br.total_tcs_price = (@selected_sale_price/100) * 0.075
    br.total_sold_price = (@selected_sale_price/100) + (br.total_tcs_price)
    br.save
    @total_tcs_price =  br.total_tcs_price/100.00
    @total_sold_price = br.total_sold_price
    @invoice_no = "SCRIPMARKET"+ @buy_request.sold_date.strftime('%F').split("-").join().to_s + @buy_request.id.to_s
    @kyc_doc = KycDocument.where(user_id:@buy_request.user_id).last
    respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'dashboard/invoice',pdf:@invoice_no}
    end
  end  


end
