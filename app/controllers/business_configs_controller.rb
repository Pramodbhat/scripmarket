class BusinessConfigsController < ApplicationController
    before_action :authenticate_user!
    before_action :setup
    def setup
        @user = current_user
    end

    def update
        if !BusinessConfig.config_update(current_user.id, business_config_params["config"])
            flash[:alert] = "Failed to update configs"
            redirect_to dashboard_business_config_path
            return
        end
        flash[:notice] = "Success"
        redirect_to dashboard_business_config_path
        return
    end

    def business_config_params
        params.permit(:config, :referrer)
    end
end
