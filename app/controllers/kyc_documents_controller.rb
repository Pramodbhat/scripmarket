class KycDocumentsController < ApplicationController
  before_action :authenticate_user!
  before_action :setup
  before_action :set_kyc_document, only: [:show, :edit, :destroy, :update]
  before_action :set_parent_kyc_document, only: [:pan, :null_cheque, :incorporation_cert,
                                                 :phonenumber, :approval, :reject,
                                                 :information,:state,:gst_number,:pan_card_number,:kyc_pan_card_number,:iec_number]

  layout "application"


  def setup
    @user = current_user
    @hide_side_bar = !current_user.kyc_approved 
    @unknown_image = 'https://scripmarket-public.s3.ap-south-1.amazonaws.com/unknown_image.png'
    @valid_email = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  end

  # GET /kyc_documents
  # GET /kyc_documents.json
  def index
    @kyc_documents = KycDocument.includes(:user).includes(:approved_by).all
  end

  # GET /kyc_documents/1
  # GET /kyc_documents/1.json
  def show
    @pan_url = pan_url
    @null_cheque_url = null_cheque_url
    @incorporation_cert_url = incorporation_cert_url
  end

  def presigned_url (bucket, key)
    signer = Aws::S3::Presigner.new
    url = signer.presigned_url(:get_object, bucket: bucket, key: key)
  end

  def pan_url()
    return @unknown_image unless (@kyc_document.pan_card and @kyc_document.pan_card.length)
    presigned_url('scripmarket-kyc', @kyc_document.pan_card)
  end

  def null_cheque_url()
    return @unknown_image unless (@kyc_document.null_cheque and @kyc_document.null_cheque.length)
    presigned_url('scripmarket-kyc', @kyc_document.null_cheque)
  end

  def incorporation_cert_url()
    return @unknown_image unless (@kyc_document.incorporation_cert and @kyc_document.incorporation_cert.length)
    presigned_url('scripmarket-kyc', @kyc_document.incorporation_cert)
  end

  # GET /kyc_documents/new
  def new
    open_documents = KycDocument.where("user_id = ? and (status != ?)", current_user.id, 'INVALID')
    if open_documents.length > 0
      @kyc_document = open_documents.last 
    else
      @kyc_document = KycDocument.create(user_id: current_user.id, status: "DRAFT")
    end
    @pan_url = pan_url
    @null_cheque_url = null_cheque_url
    @incorporation_cert_url = incorporation_cert_url

  end

  # GET /kyc_documents/1/edit
  def edit
    @kyc_document.pan_card = kyc_document_params
  end

  # POST /kyc_documents/1/pan
  def pan
    @kyc_document.pan_card = params[:key]
    @kyc_document.save
    respond_to do |format|
      format.json {
        render json: {"url" => pan_url()}
      }
    end
  end

  # POST /kyc_documents/1/pan
  def null_cheque
    @kyc_document.null_cheque = params[:key]
    @kyc_document.save
    respond_to do |format|
      format.json {
        render json: {"url" => null_cheque_url()}
      }
    end
  end

  # POST /kyc_documents/1/pan
  def incorporation_cert
    @kyc_document.incorporation_cert = params[:key]
    @kyc_document.save
    respond_to do |format|
      format.json {
        render json: {"url" => incorporation_cert_url()}
      }
    end
  end

  def phonenumber
    @kyc_document.phonenumber = params[:key]
    @kyc_document.save
    respond_to do |format|
      format.json {
        render json: {"phonenumber" => @kyc_document.phonenumber}
      }
    end
  end

  def approval
    if current_user.is_super_admin? 
      @kyc_document.status = "APPROVED"
      @kyc_document.rejection_reason = nil
      @kyc_document.approved_by_id = current_user.id
      @kyc_document.approved_on = Time.now
      @kyc_document.save
      owner = User.find(@kyc_document.user_id)
      owner.kyc_approved = true
      owner.save
      return redirect_to kyc_documents_path
    end

    if current_user.role == 'user'
      if current_user.id != @kyc_document.user_id
        render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
        return
      end

      if !validate_kyc_information(@kyc_document)
        flash[:alert] = @errors
      else  
        @kyc_document.status = "SUBMITTED"
        @kyc_document.save
      end  
      return redirect_to new_kyc_document_path
    elsif current_user.role = 'ops'
      @kyc_document.status = "APPROVED"
      @kyc_document.rejection_reason = nil
      @kyc_document.approved_by_id = current_user.id
      @kyc_document.approved_on = Time.now
      @kyc_document.save
      owner = User.find(@kyc_document.user_id)
      owner.kyc_approved = true
      owner.save
      return redirect_to kyc_documents_path
    end
    render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
  end

  def reject
    if current_user.role == 'ops'
      @notice = "done"
      if params["message"].nil? or params["message"].empty?
        flash[:alert] = "Need a rejection reason."
        return redirect_to kyc_document_url(@kyc_document)
      end
      @kyc_document.rejection_reason = params[:message]
      @kyc_document.status = "REJECTED"
      @kyc_document.approved_by_id = nil
      @kyc_document.approved_on = nil
      @kyc_document.save
      return redirect_to kyc_documents_path
    end
    render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
  end
  # POST /kyc_documents
  # POST /kyc_documents.json
  def create
    @kyc_document = KycDocument.new(kyc_document_params)

    respond_to do |format|
      if @kyc_document.save
        format.html { redirect_to @kyc_document, notice: 'Kyc document was successfully created.' }
        format.json { render :show, status: :created, location: @kyc_document }
      else
        format.html { render :new }
        format.json { render json: @kyc_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kyc_documents/1
  # PATCH/PUT /kyc_documents/1.json
  def update
    #validate_kyc_information(kyc_document_params)
    respond_to do |format|
      if @kyc_document.update(kyc_document_params)
        format.json { render :new, status: :ok, location: @kyc_document }
      else
        format.json { render json: @kyc_document.errors, status: :unprocessable_entity }
      end
    end
  end

  def validate_kyc_name(name)
    if name.present?
      name.strip.length > 0
    else
      false
    end  
  end

  def validate_kyc_company_address(company_address)
    if company_address.present?
      company_address.strip.length > 0
    else
      false
    end  
  end

  def validate_kyc_official_email(official_email)
    if official_email.present?
      @valid_email.match(official_email) != nil
    else
      false
    end  
  end

  def validate_kyc_company_name(company_name)
    if company_name.present?
      company_name.strip.length > 0
    else
      false
    end  
  end

  def validate_kyc_phonenumber(phonenumber)
    if phonenumber.present?
      phonenumber.strip.length > 0 and /^[0-9+]+$/.match(phonenumber.strip)
    else
      false
    end  
  end

  def validate_kyc_information(values)
    @errors=""
    @errors << "Invalid name. Cannot be empty.\n" unless validate_kyc_name(values["name"])
    @errors << "Invalid company address. Cannot be empty.\n" unless validate_kyc_company_address(values["company_address"])
    @errors << "Invalid official email.\n" unless validate_kyc_official_email(values["official_email"])
    @errors << "Invalid company name." unless validate_kyc_company_name(values["company_name"])
    @errors << "Invalid phone number only numbers, '+' allowed" unless validate_kyc_phonenumber(values["phonenumber"])
    @errors << "Invalid GST Number" unless validate_kyc_company_name(values["gst_number"])
    @errors << "Invalid PAN Card Number." unless validate_kyc_company_name(values["kyc_pan_card_number"])
    @errors << "Invalid IEC Number." unless validate_kyc_company_name(values["iec_number"])
    @errors << "Invalid State." unless validate_kyc_company_name(values["state"])
    @errors.length == 0
  end

  def information
    respond_to do |format|
      format.json { 
        if !validate_kyc_information(kyc_document_params)
          return render json: { error: @errors, success: false}, status: :unprocessable_entity
        end
        kyc_params = kyc_document_params
        @kyc_document.name = kyc_params["name"].strip
        @kyc_document.company_address = kyc_params["company_address"].strip
        @kyc_document.official_email = kyc_params["official_email"].strip
        @kyc_document.company_name = kyc_params["company_name"].strip
        @kyc_document.phonenumber = kyc_params["phonenumber"].strip
        @kyc_document.state = kyc_params["state"].strip
        @kyc_document.gst_number = kyc_params["gst_number"].strip
        @kyc_document.kyc_pan_card_number = kyc_params["kyc_pan_card_number"].strip
        @kyc_document.iec_number = kyc_params["iec_number"].strip
        @kyc_document.save!
        return render json: { success: true }, status: :ok
      }
    end
  end

  # DELETE /kyc_documents/1
  # DELETE /kyc_documents/1.json
  def destroy
    @kyc_document.destroy
    respond_to do |format|
      format.html { redirect_to kyc_documents_url, notice: 'Kyc document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  def oops_lock
      @kyc_document = KycDocument.find(params[:kyc_document_id])
      @kyc_document.locked_by_user_id = current_user[:id]
      @kyc_document.is_locked = true
      if @kyc_document.save
        flash[:alert] = "Kyc Document is Locked Successfully"
          return redirect_to kyc_documents_path
      end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kyc_document
      @kyc_document = KycDocument.find(params[:id])
    end

    def set_parent_kyc_document
      @kyc_document = KycDocument.find(params[:kyc_document_id])
    end

    # Only allow a list of trusted parameters through.
    def kyc_document_params
      params.permit(:pan_card, :null_cheque, 
        :incorporation_cert, :user_id, :approved_by_id, :status, :approved_on, 
        :rejection_reason, :valid_till, :phonenumber,
        :name, :company_name, :company_address, :official_email,:state,:gst_number,:pan_card_number,:kyc_pan_card_number,:iec_number)
    end
end
