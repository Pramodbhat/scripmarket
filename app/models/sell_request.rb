class SellRequest < ApplicationRecord
    belongs_to :user
    validates :scrip_number, uniqueness: { scope: [:user_id] }
    def self.active_sales(user_id)
      user = User.find user_id
      if user[:role] == "ops"
        SellRequest.where("verified = true AND sold = false AND deleted = false")
      else
        SellRequest.where("user_id = :user_id AND verified = true AND sold = false AND deleted = false", {user_id: user_id})
      end  
    end

    def self.unverified_sales(user_id)
      user = User.find user_id
      if user[:role] == "ops"
        SellRequest.where("verified = false AND sold = false AND deleted = false")
      else
      SellRequest.where("user_id = :user_id AND verified = false AND sold = false AND deleted = false", {user_id: user_id})
      end 
    end

    def self.completed_sales(user_id)
      user = User.find user_id
      if user[:role] == "ops"
        SellRequest.where("verified = true AND sold = true AND deleted = false")
      else
        SellRequest.where("user_id = :user_id AND verified = true AND sold = true AND deleted = false", {user_id: user_id})
      end
    end

    def self.for_verification
        SellRequest.where("verified = false AND sold = false AND deleted = false AND rejected = false")
    end

    def self.rejected_sales
        SellRequest.where("verified = false AND sold = false AND deleted = false AND rejected = true")
    end

    def self.active_sales_all
        SellRequest.where("verified = true AND sold = false AND deleted = false")
    end


    def self.number_to_indian_currency(number, html=true)
     txt = html ? content_tag(:span, 'Rs.', :class => :WebRupee) : ' ' 
     "#{txt} #{number.to_s.gsub(/(\d+?)(?=(\d\d)+(\d)(?!\d))(\.\d+)?/, "\\1,")}"
    end 


end
