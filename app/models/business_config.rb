class BusinessConfig < ApplicationRecord
  belongs_to :user
  
  def self.get_latest(user_id)
    business_config = BusinessConfig.where(enabled: true).last
    if business_config == nil
      business_config = BusinessConfig.new
      business_config.user_id = user_id
      business_config.save!
    end
    business_config
  end

  def self.config_update(user_id, config)
    old = BusinessConfig.get_latest(user_id)
    copy = BusinessConfig.new
    copy.user_id = user_id
    copy.enabled = true
    copy.config = JSON.parse config

    old.enabled = false
    return false unless copy.save
    
    return true if old.save
      
    BusinessConfig.destroy(copy.id)
    return false
  end
end
