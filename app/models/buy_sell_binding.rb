class BuySellBinding < ApplicationRecord
    belongs_to :buy_request, foreign_key: 'buy_request_id'
    belongs_to :sell_request, foreign_key: 'sell_request_id'
end
