class KycDocument < ApplicationRecord
  belongs_to :user
  # removed approved_by because it caused approved_by to not be nullable
  belongs_to :approved_by, :class_name => 'User', optional: true


end
