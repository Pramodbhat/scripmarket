class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable
  has_many :kyc_documents, :class_name => 'KycDocument', :foreign_key => 'user_id'
  has_many :approved_kyc_documents, :class_name => 'KycDocument', :foreign_key => 'approved_by_id'
  has_one :user_attributes, :class_name => 'UserAttribute', :foreign_key => 'user_id'
  has_many :sell_requests, :class_name => 'SellRequest', :foreign_key => 'verified_by'
  
  def can?(permission)
    self.user_attributes.can?(permission)
  end

  def is_super_admin?
    ["pramodbha8@gmail.com"].split(',').flatten.include?(self.email)
  end
end
