class UserAttribute < ApplicationRecord
    belongs_to :user
    
    def self.can?(user_id, permission)
        UserAttribute.find_by(user_id: user_id).preferences[permission]
    end

    def can?(permission)
        self.preferences[permission]
    end

    def self.find_or_create(user_id)
        attributes = UserAttribute.find_by(user_id: user_id)
        return attributes unless attributes.nil?
        
        attributes = UserAttribute.new
        attributes.user = User.new
        attributes.user.id = user_id
        return nil unless attributes.save
        
        attributes
    end
end
