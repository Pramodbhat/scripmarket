module Validators
  class BuyValidator < ActiveModel::Validator
      def validate(record)
        if record.buy_date.nil?
          record.errors[:buy_date] << 'Date cannot be empty'
          return
        end
        
        unless record.buy_date >= Date.today
          record.errors[:buy_date] << 'Date cannot be in the past'
        end

        unless record.scrip_value > 0 and record.scrip_value < 100000000000
          record.errors[:scrip_value] << 'scrip value should be greater than 0 and less than 100 crore'      
        end
      end
  end
end