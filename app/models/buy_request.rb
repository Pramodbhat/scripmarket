require 'validators/buy_validator'

class BuyRequest < ApplicationRecord
    belongs_to :user
    has_many :buy_sell_bindings, foreign_key: :buy_request_id
    has_many :sell_requests, through: :buy_sell_bindings, source: 'sell_request'
    include ActiveModel::Validations
    validates_with Validators::BuyValidator

    def self.active_buys(user)
      if user[:role] == "ops"
        BuyRequest.where("deleted = false and sold = false")
      else
        BuyRequest.where("user_id= :user_id and deleted = false and sold = false", user_id: user.id)
      end
    end

    def self.completed_buys(user)
      if user[:role] == "ops"
        BuyRequest.where("deleted = false and sold = true")
      else
        BuyRequest.where("user_id= :user_id and deleted = false and sold = true", user_id: user.id)
      end
    end

    def self.open_audit(user)
      BuyRequest.where("deleted = false and is_auidted = false and sold = true")
    end

    def self.accepted_audit(user)
      BuyRequest.where("deleted = false and is_auidted = true and sold = true and is_audit_rejected = false")
    end
    def self.rejected_audit(user)
      BuyRequest.where(" is_audit_rejected = true")
    end


    def self.active_buys_submitted
        BuyRequest.where("deleted = false and sold = false and submitted = true")
    end

    def self.is_it_sumitted(yes_no_validator)
      (yes_no_validator.present? && yes_no_validator == true) ? "Yes" : "No"
    end  
end

