function update_nett_price(event) {
    let scrip_value_str = $('#sell_request_scrip_value').val();
    let rate_str = $('#sell_request_rate').val();

    let float_pattern = /^[0-9]+[.]{0,1}[0-9]*$/;
    if (! float_pattern.test(scrip_value_str) || !(float_pattern.test(rate_str))) {
        return;
    }
    let scrip_value = parseFloat(scrip_value_str);
    let rate = parseFloat(rate_str);
    let sales_price = (scrip_value*rate)/100;
    let commission = calculate_commission(scrip_value, rate)/100;
    console.log("Calculated commission", commission);
    let commission_value = ((scrip_value*commission)/100);
    let nett_price = sales_price - commission_value;
    $("#sell_request_commission").val(commission);
    $("#sell_request_sales_price").val(sales_price);
    $("#sell_request_nett_price").val(nett_price);
    $("#sell_request_commission_value").val(commission_value);
}

function calculate_commission(scrip_value, rate) {
    config = JSON.parse($('#sell_request_commission').attr("data"));
    result = 20;
    config.forEach((x) => {
        min = x["min"]/100;
        max = x["max"];
        if (max != -1) {
            max = x["max"]/100;
        }
        if (scrip_value >= min && 
            ((scrip_value < max) || (max == -1))) {
            result = x["value"];
        }
    });
    return result; //default value
}

$(document).on("ready turbolinks:load",
    function() {
        $(document).on('keyup', "#sell_request_rate", update_nett_price);
        $(document).on('keyup', "#sell_request_scrip_value", update_nett_price);
        update_nett_price(null);
    }
);