// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require('jquery')
require('kyc_documents')
require('sell_requests')
// require('bootstrap')
// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

function toggle_menu() {
    $('#heading-menu').toggle();
}

function toggle_alert() {
    $('.alert').toggle();
}

function dashboard_button_click(event) {
        location.assign($(event.target).parent().attr('url'));
}

$(document).ready(
    function() {
        $(document).on('click','#menu-button', {}, toggle_menu);
        $(document).on('click','.alert', {},toggle_alert);
        $(document).on('click','.dashboard-home-button', {},
            dashboard_button_click);
    }
);