import $ from 'jquery';

function set_image(image_id, image_url) {
    $("#kyc-document-images-message").hide();
    $(`img#${image_id}`).attr('src', image_url);
}

function update_uploaded_image(kyc_id, typename, image_id, key) {
    let url = `/kyc_documents/${kyc_id}/${typename}/`;
    let formData = new FormData();
    formData.append("authenticity_token", $('meta[name="csrf-token"]').attr("content"));
    formData.append("key", key);
    fetch(url, {method: "POST", body: formData})
        .then(response => response.json())
        .then(imageData => set_image(image_id, imageData.url))
        .catch((reason) => {
            $('#kyc-document-images-message').text("Error. Please try again.")
        })

}

function upload_file(id, kyc_id, image_id, url, fields, key, typename) {
    let file = document.getElementById(id).files[0];
    let formData = new FormData();

    for ( var fkey of Object.keys(fields)) {
        formData.append(fkey, fields[fkey]);
    }

    formData.append("file", file);
    fetch(url, {method: "POST", body: formData})
        .then(data => update_uploaded_image(kyc_id, typename, image_id, key))
        .catch((reason) => {
            $('#kyc-document-images-message').text("Error. Please try again.")
        })

}

function upload_to_s3 (id, image_id, typename) {
    return (function(event){
        let kyc_id = $(event.target).attr("kyc_id");
        let file = document.getElementById(id).files[0];
        if (file == null || (file && file.length == 0)) {
            alert("Please input a proper file.");
            return;
        }

        $("#kyc-document-images-message").text("loading...").show();

        fetch(`/s3url?filename=${encodeURI(file.name)}&type=${encodeURI(typename)}`)
            .then(response => response.json())
            .then(urldata => upload_file(id, kyc_id, image_id, urldata.url, urldata.data,
                                         urldata.key, typename))
            .catch((reason) => {
                $('#kyc-document-images-message').text("Error. Please try again.")
            })
    });
}

function upload_phonenumber() {
    return function() {
        let kyc_id = $(event.target).attr("kyc_id");
        let url = `/kyc_documents/${kyc_id}/phonenumber/`;
        let formData = new FormData();
        formData.append("authenticity_token", $('meta[name="csrf-token"]').attr("content"));
        formData.append("key", $.trim($('textarea#kyc-phonenumber-text').val()));
        fetch(url, {method: "POST", body: formData})
            .then(response => response.json());
    };
}

function upload_information_response(response) {
    if (!response["success"]) {
        $('#kyc-document-information-message').text(response["error"]).show();
    } else {
        $('#kyc-document-information-message').hide();
    }
}

function upload_information() {
    return function(event) {
        let kyc_id = $(event.target).attr("kyc_id");
        let url = `/kyc_documents/${kyc_id}/information/`;
        let formData = new FormData();
        $('#kyc-document-information-message').text("..uploading").toggle();
        formData.append("authenticity_token", $('meta[name="csrf-token"]').attr("content"));
        formData.append("name", $('#kyc-document-information-name').val());
        formData.append("official_email", $('#kyc-document-information-official-email').val());
        formData.append("company_name", $('#kyc-document-information-company-name').val());
        formData.append("company_address", $('#kyc-document-information-company-address').val());
        formData.append("phonenumber", $('#kyc-document-information-phonenumber').val());
        formData.append("gst_number", $('#kyc-document-information-gst_number').val());
        formData.append("state", $('#kyc-document-information-state').val());
        formData.append("kyc_pan_card_number", $('#kyc-document-information-kyc_pan_card_number').val());
        formData.append("iec_number", $('#kyc-document-information-iec_number').val());
        fetch(url, {method: "POST", body: formData})
            .then(response => response.json())
            .then(responseJson => upload_information_response(responseJson));
        event.preventDefault();
    }
}

function kyc_document_view_show(event) {
    $('#kyc-document-view-image').attr("src", $(event.target).attr("src"));
    $('#kyc-document-view').show();
}

function kyc_document_view_hide(event) {
    $('#kyc-document-view').hide();
}

$(document).ready(
    function() {
        $(document).on('click','button#pan-upload', {}, upload_to_s3('pan-upload-file',
                                                  'pan-image', 'pan'));
        $(document).on('click', 'button#null-cheque-upload', {}, upload_to_s3('null-cheque-upload-file',
                                                          'null-cheque-image', 'null_cheque'));
        $(document).on('click', 'button#incorporation-cert-upload', {},
            upload_to_s3('incorporation-cert-upload-file',
                         'incorporation-cert-image',
                         'incorporation_cert'));

        $(document).on('click', 'button#phonenumber-upload', {},
            upload_phonenumber());
        
        $(document).on('click', 'button#kyc-document-information-submit', {},
            upload_information()
        );

        $(document).on('click', '.kyc-document-image', {}, kyc_document_view_show);
        $(document).on('click','#kyc-document-view-image', {}, kyc_document_view_hide);
    }
);
