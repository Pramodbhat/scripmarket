Rails.application.routes.draw do
  resources :buy_requests do
    post 'submit', action: :submit
    post 'is_sold', action: :is_sold
    get 'lock_buy', action: :lock_buy
    post 'audit', action: :audit



    post 'audit_reject', action: :audit_reject
    get 'reject_buy', action: :reject_buy
  end
  resources :sell_requests do
    post 'resubmit', action: :resubmit
    post 'upload_invoice', action: :upload_invoice
  end
  get 'dashboard/home', action: :home, controller: 'dashboard'
  get 'dashboard/sell', action: :sell, controller: 'dashboard'
  get 'dashboard/buy', action: :buy, controller: 'dashboard'
  get 'dashboard/business_config', action: :business_config, controller: 'dashboard'
  get 'dashboard/select_scrips_user', action: :select_scrips_user, controller: 'dashboard'
  get 'dashboard/select_scrips_ops', action: :select_scrips_ops, controller: 'dashboard'

  get 'dashboard/download_pdf', action: :download_pdf, controller: 'dashboard'


  get 'dashboard/verify_buy', action: :verify_buy, controller: 'dashboard'
  get 'dashboard/audit_dashboard', action: :audit_dashboard, controller: 'dashboard'
  get 'dashboard/procees_to_sell', action: :procees_to_sell, controller: 'dashboard'


  resources :permissions, only: [:index, :edit, :update]

  resources :buy_sell_binding, only: [:create, :destroy]

  get 'verify_sell_rejections', action: :rejections, controller: 'verify_sell'

  resources :verify_sell, only: [:index, :show] do
    post 'verify', action: :verify
    post 'reject', action: :reject
    get 'request_lock', action: :request_lock

  end
  get 'dashboard/invoices', action: :invoices, controller: 'dashboard'

  get 'dashboard/buy_request_audit', action: :buy_request_audit, controller: 'dashboard'

  resources :kyc_documents do
    post 'pan', action: :pan
    post 'null_cheque', action: :null_cheque
    post 'incorporation_cert', action: :incorporation_cert
    post 'phonenumber', action: :phonenumber
    post 'approval', action: :approval
    post 'reject', action: :reject
    post 'information', action: :information
    get 'oops_lock', action: :oops_lock
  end
  resources :s3url, only: [:index]
  devise_for :users, controllers: { sessions: "users/sessions", registrations: "users/registrations", confirmations: 'confirmations' }
  root to: "home#index"
  get 'kyc', action: :show, controller: 'kyc', as: 'kyc'
  get 'logout', action: :logout, controller: 'application', as: 'logout'
  resources :business_configs, only: [ :update ]

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
