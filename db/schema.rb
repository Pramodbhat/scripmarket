# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_14_041805) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "business_configs", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.boolean "enabled", default: true
    t.jsonb "config", default: {"commission"=>[{"max"=>-1, "min"=>0, "value"=>20}]}
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_business_configs_on_user_id"
  end

  create_table "buy_requests", force: :cascade do |t|
    t.integer "user_id", null: false
    t.boolean "deleted", default: false, null: false
    t.boolean "sold", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "buy_date", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "sold_date"
    t.bigint "scrip_value", default: 0
    t.boolean "submitted", default: false, null: false
    t.string "descrption"
    t.string "sold_user_email"
    t.integer "locked_by_user_id"
    t.boolean "is_locked", default: false, null: false
    t.integer "sold_user_id"
    t.boolean "is_auidted", default: false, null: false
    t.integer "audited_by"
    t.boolean "is_audit_rejected", default: false, null: false
    t.boolean "audit_rejection_message", default: false, null: false
    t.integer "tcs_commision", default: 0
    t.integer "total_sold_price"
    t.integer "total_tcs_price"
    t.integer "total_selected_scrip_value", default: 0, null: false
    t.integer "total_selected_scrip_sale_price", default: 0, null: false
    t.integer "total_amount_saved", default: 0, null: false
    t.index ["user_id"], name: "index_buy_requests_on_user_id"
  end

  create_table "buy_sell_bindings", force: :cascade do |t|
    t.bigint "buy_request_id"
    t.bigint "sell_request_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "buy_commission", default: 0, null: false
    t.index ["buy_request_id"], name: "index_buy_sell_bindings_on_buy_request_id"
    t.index ["sell_request_id"], name: "index_buy_sell_bindings_on_sell_request_id"
  end

  create_table "documents", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "type"
    t.string "bucket"
    t.string "key"
    t.string "status"
    t.string "reasons"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_documents_on_user_id"
  end

  create_table "kyc_documents", force: :cascade do |t|
    t.text "pan_card"
    t.text "null_cheque"
    t.text "incorporation_cert"
    t.bigint "user_id", null: false
    t.bigint "approved_by_id"
    t.text "status"
    t.datetime "approved_on"
    t.text "rejection_reason"
    t.datetime "valid_till"
    t.text "phonenumber"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
    t.string "company_name"
    t.string "company_address"
    t.string "official_email"
    t.integer "locked_by_user_id"
    t.boolean "is_locked", default: false, null: false
    t.string "gst_number"
    t.string "state"
    t.string "kyc_pan_card_number"
    t.string "iec_number"
    t.index ["status"], name: "index_kyc_documents_on_status"
    t.index ["user_id"], name: "index_kyc_documents_on_user_id"
  end

  create_table "sell_requests", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "scrip_number"
    t.string "scrip_date"
    t.bigint "scrip_value"
    t.integer "rate"
    t.boolean "verified", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "sold", default: false
    t.string "rejection_reason"
    t.boolean "deleted", default: false
    t.boolean "rejected", default: false
    t.integer "verified_by"
    t.integer "commission", default: 20
    t.integer "locked_by_user_id"
    t.boolean "is_locked", default: false, null: false
    t.index ["user_id", "scrip_number"], name: "index_sell_requests_on_user_id_and_scrip_number", unique: true
    t.index ["user_id"], name: "index_sell_requests_on_user_id"
    t.index ["verified"], name: "index_sell_requests_on_verified"
  end

  create_table "user_attributes", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.jsonb "preferences", default: {"buy"=>false, "sell"=>true, "sell_scrip"=>false, "user_admin"=>false, "verify_kyc"=>false, "verify_scrip"=>false}, null: false
    t.index ["user_id"], name: "index_user_attributes_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "kyc_approved", default: false
    t.datetime "kyc_valid_till"
    t.text "role", default: "user"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "business_configs", "users"
  add_foreign_key "documents", "users"
  add_foreign_key "kyc_documents", "users"
  add_foreign_key "sell_requests", "users"
  add_foreign_key "user_attributes", "users"
end
