class AddColumnCommissionToSellRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :sell_requests, :commission, :integer, default: 20
  end
end
