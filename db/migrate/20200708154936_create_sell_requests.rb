class CreateSellRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :sell_requests do |t|
      t.references :user, null: false, foreign_key: true
      t.string :scrip_number
      t.string :scrip_date
      t.bigint :scrip_value
      t.integer :rate
      t.boolean :verified, default: false
      t.timestamps
    end
    add_index :sell_requests, :verified
    add_index :sell_requests, [:user_id, :scrip_number, :verified], unique: true

  end
end
