class CreateUserAttributes < ActiveRecord::Migration[6.0]
  def change
    create_table :user_attributes do |t|
      t.references :user, null: false, foreign_key: true
      t.jsonb :preferences, null: false, default: {"buy" => false, "sell" => true, "user_admin" => false, "verify_kyc" => false, "verify_scrip" => false, "sell_scrip" => false }
    end
  end
end
