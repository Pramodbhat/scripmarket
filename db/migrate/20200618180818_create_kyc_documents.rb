class CreateKycDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :kyc_documents do |t|
      t.text :pan_card
      t.text :null_cheque
      t.text :incorporation_cert
      t.references :user, null: false, foreign_key: true
      t.references :approved_by
      t.text :status
      t.datetime :approved_on
      t.text :rejection_reason
      t.datetime :valid_till
      t.text :phonenumber

      t.timestamps
    end
    add_index :kyc_documents, :status
  end
end
