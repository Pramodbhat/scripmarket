class AddTotalSelectedScripToBuyRequest < ActiveRecord::Migration[6.0]
  def change
    add_column :buy_requests, :total_selected_scrip_value, :integer, null: false, default: 0
    add_column :buy_requests, :total_selected_scrip_sale_price, :integer, null: false, default: 0
    add_column :buy_requests, :total_amount_saved, :integer, null: false, default: 0
  end
end
