class UpdateUserAttributes < ActiveRecord::Migration[6.0]
  def change
    User.all.each do |user| 
      UserAttribute.find_or_create(user.id)
    end
  end
end
