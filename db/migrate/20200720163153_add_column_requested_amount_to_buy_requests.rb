class AddColumnRequestedAmountToBuyRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :buy_requests, :scrip_value, :bigint, default: 0
  end
end
