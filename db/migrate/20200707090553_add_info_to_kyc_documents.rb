class AddInfoToKycDocuments < ActiveRecord::Migration[6.0]
  def change
    add_column :kyc_documents, :name, :string
    add_column :kyc_documents, :company_name, :string
    add_column :kyc_documents, :company_address, :string
    add_column :kyc_documents, :official_email, :string
  end
end
