class AddColumnRejectedToSellRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :sell_requests, :rejected, :boolean, default: false
  end
end
