class CreateBuyRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :buy_requests do |t|
      t.references :user
      t.boolean :deleted
      t.boolean :sold

      t.timestamps
    end
  end
end
