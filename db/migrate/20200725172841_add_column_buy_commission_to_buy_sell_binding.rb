class AddColumnBuyCommissionToBuySellBinding < ActiveRecord::Migration[6.0]
  def change
    add_column :buy_sell_bindings, :buy_commission, :integer, null: false, default: 0
  end
end
