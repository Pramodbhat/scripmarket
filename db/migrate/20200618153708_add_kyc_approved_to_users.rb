class AddKycApprovedToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :kyc_approved, :boolean, default: false
    add_column :users, :kyc_valid_till, :datetime
  end
end
