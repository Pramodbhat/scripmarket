class CreateBusinessConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :business_configs do |t|
      t.references :user, null: false, foreign_key: true
      t.boolean :enabled, default: true
      t.jsonb :config, default: { commission: [{ min: 0, max: -1, value: 20 }]}

      t.timestamps
    end
  end
end
