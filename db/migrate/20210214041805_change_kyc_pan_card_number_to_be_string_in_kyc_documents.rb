class ChangeKycPanCardNumberToBeStringInKycDocuments < ActiveRecord::Migration[6.0]
  def change
    change_column :kyc_documents, :kyc_pan_card_number, :string
    change_column :kyc_documents, :iec_number, :string
    change_column :kyc_documents, :gst_number, :string
  end
end
