class CreateBuySellBindings < ActiveRecord::Migration[6.0]
  def change
    create_table :buy_sell_bindings do |t|
      t.references :buy_request_id
      t.references :sell_request_id

      t.timestamps
    end
  end
end
