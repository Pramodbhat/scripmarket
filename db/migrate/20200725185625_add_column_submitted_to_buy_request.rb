class AddColumnSubmittedToBuyRequest < ActiveRecord::Migration[6.0]
  def change
    add_column :buy_requests, :submitted, :boolean, null: false, default: false
  end
end
