class AddDeletedToSellRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :sell_requests, :deleted, :boolean, default: false
  end
end
