class ModifySellRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :sell_requests, 'sold', :boolean, :default => false
    remove_index :sell_requests, name: 'index_sell_requests_on_user_id_and_scrip_number_and_verified'
    add_index :sell_requests, [:user_id, :scrip_number], unique: true
  end
end
