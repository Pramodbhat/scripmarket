class FixIdsForBinding < ActiveRecord::Migration[6.0]
  def change
    rename_column :buy_sell_bindings, :buy_request_id_id, :buy_request_id
    rename_column :buy_sell_bindings, :sell_request_id_id, :sell_request_id
  end
end
