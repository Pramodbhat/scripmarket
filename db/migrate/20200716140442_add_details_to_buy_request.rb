class AddDetailsToBuyRequest < ActiveRecord::Migration[6.0]
  def change
    add_column :buy_requests, :buy_date, 'timestamp with time zone', null: false, default: -> { 'CURRENT_TIMESTAMP' }
    add_column :buy_requests, :sold_date, 'timestamp with time zone'
  end
end
