class RemoveIndexFromKycDocumentsApprovedBy < ActiveRecord::Migration[6.0]
  def change
    remove_index :kyc_documents, name: "index_kyc_documents_on_approved_by_id"
  end
end
