class AddRejectedToSellRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :sell_requests, :rejection_reason, :string
  end
end
