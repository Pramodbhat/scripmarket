class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.references :user, null: false, foreign_key: true
      t.string :type
      t.string :bucket
      t.string :key
      t.string :status
      t.string :reasons

      t.timestamps
    end
  end
end
