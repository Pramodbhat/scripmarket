class MakeBuyDeletedAndSoldDefaultFalse < ActiveRecord::Migration[6.0]
  def up
    change_column :buy_requests, :deleted, :boolean, null: false, default: false
    change_column :buy_requests, :sold, :boolean, null: false, default: false
    change_column :buy_requests, :user_id, :integer, null: false
  end

  def down
    change_column :buy_requests, :deleted, :boolean, null: false, default: false
    change_column :buy_requests, :sold, :boolean, null: false, default: false
    change_column :buy_requests, :user_id, :integer, null: false
  end
end
