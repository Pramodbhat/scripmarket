class AddDescrptionToBuyRequest < ActiveRecord::Migration[6.0]
  def change
    add_column :buy_requests, :descrption, :string
    add_column :buy_requests, :sold_user_email, :string
    add_column :buy_requests, :locked_by_user_id, :integer
    add_column :buy_requests, :is_locked, :boolean, null: false, default: false
    add_column :buy_requests, :sold_user_id, :integer
    add_column :sell_requests, :locked_by_user_id, :integer
    add_column :sell_requests, :is_locked, :boolean, null: false, default: false
    add_column :kyc_documents, :locked_by_user_id, :integer
    add_column :kyc_documents, :is_locked, :boolean, null: false, default: false
    add_column :buy_requests, :is_auidted, :boolean, null: false, default: false
    add_column :buy_requests, :audited_by, :integer
    add_column :buy_requests, :is_audit_rejected, :boolean, null: false, default: false
    add_column :buy_requests, :audit_rejection_message, :boolean, null: false, default: false
    add_column :buy_requests, :tcs_commision, :integer,default: 0.075
    add_column :buy_requests, :total_sold_price, :integer
    add_column :buy_requests, :total_tcs_price, :integer
    add_column :kyc_documents, :gst_number, :integer
    add_column :kyc_documents, :state, :string
    add_column :kyc_documents, :kyc_pan_card_number, :integer
    add_column :kyc_documents, :iec_number, :integer
  end
end
