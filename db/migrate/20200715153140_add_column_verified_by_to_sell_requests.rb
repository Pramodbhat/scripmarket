class AddColumnVerifiedByToSellRequests < ActiveRecord::Migration[6.0]
  def change
    add_column :sell_requests, :verified_by, :integer, default: nil
  end
end
